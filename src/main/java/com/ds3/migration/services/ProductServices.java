package com.ds3.migration.services;

import com.ds3.migration.models.DTOs.ProductDTORequest;
import com.ds3.migration.models.ProductModel;
import com.ds3.migration.repositories.ProductRepository;
import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.Date;
import java.util.List;
import java.util.Optional;
import java.util.UUID;

@Service
public class ProductServices {
    @Autowired
    private ProductRepository repository;


    public ProductModel saveProduct(ProductDTORequest dto){
        ProductModel product = new ProductModel();
        BeanUtils.copyProperties(dto, product);
        return repository.save(product);
    }

    public List<ProductModel> getAllProducts(){
        return repository.findAll();
    }

    public Optional<ProductModel> getByID(UUID id){
        return repository.findById(id);
    }

    public Boolean deleteProduct(UUID id){
        Optional<ProductModel> product = getByID(id);

        if (product.isEmpty()) return false;

        repository.delete(product.get());
        return true;
    }

    public ProductModel updateProduct(ProductDTORequest dto, UUID id){
        Optional<ProductModel> product = getByID(id);

        if (product.isEmpty()) return null;

        ProductModel productModel = product.get();
        BeanUtils.copyProperties(dto, productModel);
        productModel.setUpdatedAt(new Date());

        return repository.save(productModel);
    }






}
