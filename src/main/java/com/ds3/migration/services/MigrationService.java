package com.ds3.migration.services;

import com.ds3.migration.models.ProductModel;
import com.ds3.migration.models.ProductModelMongo;
import com.ds3.migration.repositories.ProductRepository;
import com.ds3.migration.repositories.ProductRepositoryMongo;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class MigrationService {

    @Autowired
    ProductRepository productRepository;

    @Autowired
    ProductRepositoryMongo productRepositoryMongo;

    public void migration(){
        List<ProductModel> productEntities = productRepository.findAll();
        for (ProductModel product:productEntities){
            ProductModelMongo productMongo = new ProductModelMongo(
                    product.getId().toString(),
                    product.getName(),
                    product.getPrice(),
                    product.getStock(),
                    product.getCreatedAt(),
                    product.getUpdatedAt());

            productRepositoryMongo.save(productMongo);
        }
    }
}
