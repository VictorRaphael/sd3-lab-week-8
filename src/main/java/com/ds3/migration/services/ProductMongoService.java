package com.ds3.migration.services;

import com.ds3.migration.models.ProductModelMongo;
import com.ds3.migration.repositories.ProductRepositoryMongo;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class ProductMongoService {
    @Autowired
    ProductRepositoryMongo repositoryMongo;

    public List<ProductModelMongo> getAllProducts(){
        return  repositoryMongo.findAll();
    }
}
