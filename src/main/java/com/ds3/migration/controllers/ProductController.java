package com.ds3.migration.controllers;


import com.ds3.migration.models.DTOs.ProductDTORequest;
import com.ds3.migration.models.ProductModel;
import com.ds3.migration.services.ProductServices;
import io.swagger.v3.oas.annotations.Operation;
import io.swagger.v3.oas.annotations.tags.Tag;
import jakarta.validation.Valid;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.List;
import java.util.Optional;
import java.util.UUID;

@RestController
@RequestMapping("/products")
@CrossOrigin(origins = "*", maxAge = 3600)
@Tag(name = "Product-Controller")
public class ProductController {

    @Autowired
    ProductServices service;

    @PostMapping
    @Operation(summary = "Cria produtos no banco de dados MYSQL", method = "POST")
    public ResponseEntity<ProductModel> createdProduct(@RequestBody @Valid ProductDTORequest dto){
        return ResponseEntity.status(HttpStatus.CREATED).body(service.saveProduct(dto));
    }

    @GetMapping
    @Operation(summary = "Busca todos os produtos no banco de dados MYSQL", method = "GET")
    public ResponseEntity<List<ProductModel>> getProducts(){
        return ResponseEntity.status(HttpStatus.OK).body(service.getAllProducts());
    }

    @GetMapping("/{id}")
    @Operation(summary = "Busca um produto por ID no banco de dados MYSQL", method = "GET")
    public ResponseEntity<Object> getProduct(@PathVariable("id") UUID id){
        Optional<ProductModel> product = service.getByID(id);

        if(product.isEmpty()) return ResponseEntity.status(HttpStatus.NOT_FOUND).body("Product not found");

        return ResponseEntity.status(HttpStatus.OK).body(product.get());
    }

    @DeleteMapping("/{id}")
    @Operation(summary = "Deleta produtos no banco de dados MYSQL", method = "DELETE")
    public ResponseEntity<String> deleteProduct(@PathVariable("id") UUID id){
        boolean success = service.deleteProduct(id);
        if (success) return ResponseEntity.status(HttpStatus.OK).body("Product deleted successfully");
        return ResponseEntity.status(HttpStatus.NOT_FOUND).body("Product not found");
    }

    @PutMapping("/{id}")
    @Operation(summary = "Atualiza produtos no banco de dados MYSQL", method = "PUT")
    public ResponseEntity<Object> updateProduct(@RequestBody @Valid ProductDTORequest dto, @PathVariable("id") UUID id){
        ProductModel updateProduct = service.updateProduct(dto,id);
        if (updateProduct.equals(null)) return ResponseEntity.status(HttpStatus.NOT_FOUND).body("Product not found");

        return ResponseEntity.status(HttpStatus.OK).body(updateProduct);
    }
}
