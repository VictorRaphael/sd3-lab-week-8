package com.ds3.migration.repositories;

import com.ds3.migration.models.ProductModelMongo;
import org.springframework.data.mongodb.repository.MongoRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface ProductRepositoryMongo extends MongoRepository<ProductModelMongo, String> {

}
