package com.ds3.migration.models;

import jakarta.persistence.Column;
import jakarta.persistence.Entity;
import jakarta.persistence.Table;
import lombok.Data;

@Entity
@Table(name = "TB_PRODUCTS")
@Data
public class ProductModel extends AbstractModel {

    @Column(name = "PRODUCT_NAME", nullable = false)
    private String name;

    @Column(name = "PRODUCT_VALUE", nullable = false)
    private Double price;

    @Column(name = "PRODUCT_STOCK", nullable = false)
    private Integer stock;
}
