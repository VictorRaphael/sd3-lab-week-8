package com.ds3.migration.models;

import lombok.AllArgsConstructor;
import lombok.Data;
import org.springframework.data.mongodb.core.mapping.Document;
import org.springframework.data.mongodb.core.mapping.Field;
import org.springframework.data.mongodb.core.mapping.MongoId;

import java.util.Date;

@Document(collection = "products")
@Data
@AllArgsConstructor
public class ProductModelMongo {
    @MongoId
    private String id;

    @Field(name = "PRODUCT_NAME")
    private String name;

    @Field(name = "PRODUCT_VALUE")
    private Double price;

    @Field(name = "PRODUCT_STOCK")
    private Integer stock;

    @Field(name = "DT_CREATED_AT")
    private Date createdAt;

    @Field(name = "DT_UPDATE_AT")
    private Date updatedAt;

}
